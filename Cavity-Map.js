process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function cavityMap(grid) {
    return grid
        .map(e => e.split(''))
        .reduce((a, c, i, ar) => {
            var d = c
             .reduce((aa, cc, ii, arr) => {
                var dd = (
                    (i > 0 && i < arr.length -1) && 
                    (ii > 0 && ii < arr.length -1) && 
                    (arr[ii - 1] < cc && arr[ii + 1] < cc) && 
                    (ar[i - 1][ii] < cc && ar[i + 1][ii] < cc) 
                ) ? 'X' : cc
                aa.push(dd)
                return aa
            }, [])
            a.push(d)
            return a
        }, [])
        .map(e => e.join(''));
}

function main() {
    var n = parseInt(readLine());
    var grid = [];
    for(var grid_i = 0; grid_i < n; grid_i++){
       grid[grid_i] = readLine();
    }
    var result = cavityMap(grid);
    console.log(result.join("\n"));
}
